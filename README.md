Web-App con Angular 10
-Nicolas Ayala

### Requires 🔧

```
- NodeJS: ^12.*
- NPM
- ANGULAR CLI: >=10.*
- PHP: 7.3.12
- SLIM FRAMEWORK: ^2.* 
- BOOTSTRAP: ^4.5.0
- JQUERY: ^3.5.1
ALSO:
- MariaDB: 10.4.10
- Apache: 2.4.41 
```

```
Programa para servidor Apache local utilizado: **WAMP64** (https://www.wampserver.com/en/)
Acceder a http://localhost/phpmyadmin para gestionar lo relacionado a la BD.
```

### Configuración inicial 🛠️

```
Si ya se tienen todos los requerimientos instalados, con los archivos adjuntos ya descargados,
en la consola podemos iniciar nuestro servidor de Angular.
```
```
/* ANGULAR WEB-APP */
1- Accedemos al directorio donde están ubicados los archivos (dependiendo de la ubicación del usuario)
2- En la consola de comandos, digitamos 'npm start' o 'ng serve' para iniciar el servidor donde se aloja el proyecto.
3- Se deberia iniciar el servidor en http://localhost:4200 (puerto 4200 por defecto). Si vamos al navegador, deberia verse así:
4- Ya tenemos la web-app iniciada (en principio).
```
### IMPORTANTE 📢
```
1. Acceder al directorio de la webapp => app/services/global.ts
    Allí, está la URL del servidor Apache por defecto para realizar todas las consultas relacionadas a los productos y trabajar con ellos.
    Es **NECESARIO** modificar la URL por el servidor personal creado previamente con Apache.

2. Acceder a angular-backend/index.php
    En la variable **'$db'** se realiza la conexión con la base de datos (productos_angular, en este caso) =>
    "$db = mysqli_connect('localhost', 'root', '', 'productos_angular');"

    * "$db = mysqli_connect('url_localhost_apache', 'username', 'pass', 'nombre_bd');"
    ***** Modificarlo en caso de utilizar otro nombre para la base de datos!
    
Dentro del index.php se ejecutan todos los métodos HTTP, llamados a las tablas, etc. Por lo cuál es NECESARIO conectar correctamente a la base de datos.
```

## Capturas 📄
![consola-comandos](https://i.ibb.co/C9S0FLh/ng-serve.png "Consola")
![base-de-datos](https://i.ibb.co/RN8PZNr/bd.png "Base de datos")
![home-page](https://i.ibb.co/6yD1zZY/home.png "Home page")
![productos](https://i.ibb.co/nQDZwZh/productos.png "Productos")