import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Producto } from '../models/producto';
import { GLOBAL } from './global';


@Injectable()

export class ProductosService{
    public url:string;

    constructor(
        public _http: HttpClient
    ){
        this.url = GLOBAL.url;
    }

    // Llamada a la URL del backend
    getProductos(): Observable<any>{
        return this._http.get(this.url+'/productos');
    }

    // Agregar productos
    addProducto(producto:Producto) {
        // conversion a formato JSON
        let json = JSON.stringify(producto);
        let params = 'json='+json;
        // Forma de procesar los datos en el back end
        let headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});

        return this._http.post(this.url+'/productos', params, {headers: headers});
    }

    // File Request para las imagenes. Nueva promesa
    makeFileRequest(url:string, params:Array<string>, files:Array<File>){
        return new Promise<any>((resolve, reject) => {
            var formData: any = new FormData();
            var xhr = new XMLHttpRequest();

            for (let i = 0; i < files.length; i++){
                formData.append('uploads[]', files[i], files[i].name)
            };

            xhr.onreadystatechange = function(){
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            };

            xhr.open("POST", url, true);
            xhr.send(formData);
        });
    }

    // Solicitar ULR del producto por id
    getProducto(id){
        return this._http.get(this.url+'/productos/'+id);
    }

    // Borrar productos llamando al 'delete-producto' backend
    deleteProducto(id){
        return this._http.get(this.url+'/delete-producto/'+id);
    }
}
