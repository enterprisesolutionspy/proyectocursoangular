import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

// Componentes
import { ProductosService } from '../services/producto.services';
import { Producto } from "../models/producto";
import { Form } from '@angular/forms';
import { GLOBAL } from '../services/global';
 
@Component({
  selector: 'app-producto-add',
  templateUrl: './producto-add.component.html',
  styleUrls: ['./producto-add.component.css'],
  providers: [ProductosService]
})
export class ProductoAddComponent implements OnInit {

    public titulo:string;
    public producto: Producto;
    public retorno:any;
    public filesToUpload;
	  public resultUpload;

  constructor(
    private _productoService: ProductosService,
    private _route: ActivatedRoute,
    private _router: Router
  ) 
  { 
    this.titulo = 'Crear producto';
    // Crear objeto producto; valores por default:
    this.producto = new Producto(0, '','',0,'');
  }

  ngOnInit(): void {
    console.log("Componente de creación productos cargada")
  }


  // Función al clickear 'submit'
  onSubmit() {
    console.log(this.producto);

    // GUARDADO DE IMAGEN EN LA BASE DE DATOS
    // Solamente se ejecuta al tener 1 archivo seleccionado
    if (this.filesToUpload && this.filesToUpload.length >= 1) { 
        this._productoService.makeFileRequest(GLOBAL.url+'/upload-file', [], this.filesToUpload).then((result) => {
            console.log(result);
            this.producto.imagen =  result.filename;
            this.saveProducto();
        
          // Captura en caso de error en la consola
        }, (error) =>{
          console.log(error);
        });
      } else {
        this.saveProducto();
      }
  }

  fileChangeEvent(fileInput: any){
    this.filesToUpload = <Array<File>>fileInput.target.files;
    console.log(this.filesToUpload);
  }


  // Método para guardar el producto
  saveProducto(){
    // Método 'addProducto' declarado en producto.services
    this._productoService.addProducto(this.producto).subscribe(
      result => {
          this.retorno = result;
          // Si sale bien, retorna a la página productos
          if (this.retorno.code == 200) {
            this._router.navigate(['/productos']);
          } 
          else {
            console.log(this.retorno)
          }
      }, // Capturar cualquier error
      error => {
        console.log(<any>error);
      }
    )
  }

}
