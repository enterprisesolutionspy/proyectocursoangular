CREATE DATABASE productos_angular;
USE productos_angular;

CREATE TABLE productos
( id INT PRIMARY KEY,
  nombre VARCHAR(50) NOT NULL,
  descripcion VARCHAR(50),
  precio VARCHAR(50),
  imagen VARCHAR(50)
);