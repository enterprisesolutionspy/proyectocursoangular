<?php 

require_once 'vendor/autoload.php';


$app = new \Slim\Slim();

$db = mysqli_connect('localhost', 'root', '', 'productos_angular');

// Configuracion cabeceras HTTP
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if($method == "OPTIONS") {
    die();
}

$app->get('/productos', function() use($db, $app) {
    $sql = 'SELECT * FROM productos ORDER BY id DESC;';
    $query = $db->query($sql);
    
    $productos = array();

    while ($producto = $query->fetch_assoc()) {
        $productos[] = $producto;
    }

    $result = array(
            'status' => 'success',
            'code' => 200,
            'data' => $productos
    );

    echo json_encode($result);
});

// productos por id
$app->get('/productos/:id', function($id) use($db, $app){
    $sql = 'SELECT * FROM productos WHERE id = '.$id;
    $query = $db->query($sql);

    $result = array(
        "status" => "error",
        "code" => 404,
        "message" => "Producto no disponible"
    );
    if ($query->num_rows == 1) {
        $producto = $query->fetch_assoc();
        $result = array(
            "status" => "success",
            "code" => 200,  
            "data" => $producto
        );
    } 
    echo json_encode($result);
});

// eliminar producto

$app->get('/delete-producto/:id', function($id) use($db, $app) {
    $sql = 'DELETE  FROM productos WHERE id= '.$id;
    $query = $db->query($sql); 

    if($query) {
        $result = array(
            "status" => "success",
            "code" => 200,
            "message" => "Producto borrado correctamente"
        );
    } else {
        $result = array(
        "status" => "error",
        "code" => 404,  
        "message" => 'El producto NO ha sido borrado'
        );
    }

    echo json_encode($result);
});

// actualizar producto
$app->post('/update-producto/:id',function($id) use($db,$app){
 
    $json = $app->request->post('json');
    $data = json_decode($json,true);

    $sql = "UPDATE productos SET ".
            "nombre = '{$data["nombre"]}', ".
            "descripcion = '{$data["descripcion"]}', ";

            if(isset($data["imagen"])){
                $sql .= "imagen = '{$data["imagen"]}', ";
            }

        $sql .= "precio = '{$data["precio"]}' WHERE id = {$id}";

   
    $query = $db->query($sql);
        

    if($query){
        $result = array(
            'status' =>  'succes',
            'code' => 200,
            'message' => 'El producto se ha actualizado correctamente !!'
        );
    }
    else {
        
        $result = array(
            'status' =>  'error',
            'code' => 404,
            'message' => 'El producto NO  se ha actualizado  !!'
        );
    }
    echo json_encode($result);

});

// subir imagen de producto

$app->post('/upload-file', function() use($app, $db){
    $result = array(
        'status' =>  'error',
        'code' => 404,
        'message' => 'El archivo no se pudo subir'
    );
    if (isset($_FILES['uploads'])){
        $piramideUploader = new PiramideUploader();
        $upload = $piramideUploader->upload('image','uploads','uploads', array('image/jpeg','image/png'));
        $file = $piramideUploader->getInfoFile();
        $file_name = $file['complete_name'];

        if(isset($upload) && $upload["uploaded"] == false){
            $result = array(
                'status' =>  'error',
                'code' => 404,
                'message' => 'El archivo no se pudo subir'
            );
        } else {
            $result = array(
                'status' =>  'success',
                'code' => 200,
                'message' => 'El archivo se ha subido correctamente',
                'filename' => $file_name
            );
        }
    }

    echo json_encode($result);
});



$app->post("/productos", function() use($app, $db){
    
    $json = $app->request->post('json');
    $data = json_decode($json, true);

    $nom= $data['nombre'];
    $desc= $data['descripcion'];
    $prec= $data['precio'];
    $im= $data['imagen'];
    
    $query= "INSERT INTO `productos` (`id`, `nombre`, `descripcion`, `precio`, `imagen`) 
    VALUES(NULL,'$nom','$desc','$prec','$im')";

     
        $insert = $db->query($query);

        $result = array(
            'status' => 'error',
            'code' => 404,
            'message' => 'Producto NO se ha creado correctamente'
        );
 
        if($insert){
            $result = array(
                'status' => 'success',
                'code' => 200,
                'message' => 'Producto Creado correctamente'
            );
        }
        echo json_encode($result);
});
$app->run();